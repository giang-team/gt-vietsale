import { Notify } from 'quasar';

Notify.setDefaults({
  position: 'bottom',
  timeout: 2500,
  textColor: 'white',
  actions: [{ icon: 'close', color: 'white' }],
});

export default async ({ Vue }) => {
  Vue.prototype.$notify = {
    success: (message, position = 'bottom', timeout = 2500) => {
      Notify.create({ color: 'positive', message, position, timeout });
    },
    error: (message, position = 'bottom') => {
      Notify.create({ color: 'negative', message, position });
    },
  };
};
