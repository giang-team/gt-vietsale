import * as firebase from 'firebase/app';

import 'firebase/auth';
import 'firebase/firestore';
import '@firebase/messaging';

function configFirebase(store) {
  const firebaseConfig = {
    apiKey: 'AIzaSyB8K-1CFuQIWt2H4o2p4y-OPVX_CeAQA2M',
    authDomain: 'shopdemo-3b115.firebaseapp.com',
    databaseURL: 'https://shopdemo-3b115.firebaseio.com',
    projectId: 'shopdemo-3b115',
    storageBucket: 'shopdemo-3b115.appspot.com',
    messagingSenderId: '65230351600',
    appId: '1:65230351600:web:f0832193eba5e83510a635',
  };

  if (firebase.messaging.isSupported()) {
    firebase.initializeApp(firebaseConfig);
    const messaging = firebase.messaging();

    navigator.serviceWorker.register(process.env.SERVICE_WORKER_FILE).then((registration) => {
      messaging.useServiceWorker(registration);
      messaging
        .requestPermission()
        .then(() => messaging.getToken())
        .then((token) => {
          store.dispatch('notifications/updateFcmToken', token);
        });
    });
  }
}

export default ({ store }) => {
  if (window.FirebasePlugin) {
    window.FirebasePlugin.grantPermission((hasPermission) => {
      if (hasPermission) {
        window.FirebasePlugin.onTokenRefresh((token) => {
          store.dispatch('notifications/updateFcmToken', token);
        });
        window.FirebasePlugin.onMessageReceived((message) => {
          if (message.messageType === 'notification') {
            window.cordova.plugins.notification.badge.set(10);
          }
        });
      }
    });
  } else {
    if (!window.Notification) {
      return;
    }

    if (Notification.permission === 'granted') {
      configFirebase(store);
      return;
    }

    if (!Notification.requestPermission()) {
      return;
    }

    Notification.requestPermission().then((permission) => {
      if (permission === 'granted') {
        configFirebase(store);
      }
    });
  }
};
