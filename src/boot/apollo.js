import _merge from 'lodash/merge';
import { Loading } from 'quasar';
import gql from 'graphql-tag';
import { ApolloClient } from 'apollo-client';
import { split, ApolloLink } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';
import { InMemoryCache } from 'apollo-cache-inmemory';

const env = process.env.NODE_ENV || 'development';

// Create an http link:
const httpLink = new HttpLink({
  uri: process.env.API_URL[env],
});

// Create a WebSocket link:
const wsLink = new WebSocketLink({
  uri: process.env.SUBSCRIPTION_URL[env],
  options: {
    reconnect: true,
  },
});

const authLink = new ApolloLink((operation, forward) => {
  const token =
    localStorage.getItem('token') || localStorage.getItem('admin_token');
  // Use the setContext method to set the HTTP headers.
  operation.setContext({
    headers: {
      authorization: token ? `Bearer ${token}` : '',
    },
  });

  // Call the next link in the middleware chain.
  return forward(operation);
});

// using the ability to split links, you can send data to each link
// depending on what kind of operation is being sent
const link = split(
  // split based on operation type
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    );
  },
  wsLink,
  httpLink,
);

const apolloClient = new ApolloClient({
  link: authLink.concat(link),
  cache: new InMemoryCache(),
});

export default async ({ Vue }) => {
  const defaultOptions = {
    hasLoading: true,
    showAlert: true,
  };

  Vue.prototype.$apollo = {
    async query(query, options = {}) {
      const opts = _merge(defaultOptions, options);

      if (opts.hasLoading) {
        Loading.show();
      }

      let result = {};

      try {
        result = await apolloClient.query({
          query: gql`
            ${query}
          `,
          fetchPolicy: 'network-only',
        });
      } catch (error) {
        if (opts.showAlert) {
          if (error.graphQLErrors && error.graphQLErrors.length) {
            Vue.prototype.$notify.error(error.graphQLErrors[0].message);
          }
        }
      }

      if (opts.hasLoading) {
        Loading.hide();
      }

      return result.data;
    },
    async mutation(mutation, options = {}) {
      const opts = _merge(defaultOptions, options);

      if (opts.hasLoading) {
        Loading.show();
      }

      let result = {};

      try {
        result = await apolloClient.mutate({
          mutation: gql`
            ${mutation}
          `,
        });
      } catch (error) {
        if (opts.showAlert) {
          if (error.graphQLErrors && error.graphQLErrors.length) {
            Vue.prototype.$notify.error(error.graphQLErrors[0].message);
          }
        }
      }

      if (opts.hasLoading) {
        Loading.hide();
      }

      return result.data;
    },
    subscribe(query) {
      return apolloClient.subscribe({
        query: gql`
          ${query}
        `,
        fetchPolicy: 'network-only',
      });
    },
  };
};
