import moment from 'moment';
import { TABLE_DETAIL_TEXT, USER_STATUS_TEXT, USER_GENDER_TEXT, ORDER_STATUS_TEXT, USER_RETAIL_TEXT } from '../commons/enums';
import { CommonUtils } from '../utils/commons';

export default async ({ Vue }) => {
  Vue.filter('table-detail', key => TABLE_DETAIL_TEXT[key] || '');
  Vue.filter('user-status', status => USER_STATUS_TEXT[status] || '');
  Vue.filter('order-status', status => ORDER_STATUS_TEXT[status] || '');
  Vue.filter('user-gender', gender => USER_GENDER_TEXT[gender] || '');
  Vue.filter('user-retail', retail => USER_RETAIL_TEXT[retail] || '');
  Vue.filter('moment', date => moment(date).format('DD/MM/YYYY HH:mm:ss'));
  Vue.filter('moment-from-now', date => moment(date).locale('vi').fromNow());
  Vue.filter('money', money => `${parseInt(money || 0, 10).toLocaleString()} VND`);
  Vue.filter('html', html => CommonUtils.decodeHtml(html));
};
