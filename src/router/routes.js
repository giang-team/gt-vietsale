import Vue from 'vue';

function guardAuth(to, from, next) {
  const accessToken = localStorage.getItem('token');
  if (accessToken) {
    Vue.prototype.previousRoute = from;
    next();
  } else {
    next({ path: '/login', replace: true });
  }
}

function guardAdminAuth(to, from, next) {
  const accessToken = localStorage.getItem('admin_token');
  if (accessToken) {
    Vue.prototype.previousRoute = from;
    next();
  } else {
    next({ path: '/admin/login', replace: true });
  }
}

function publicAuth(to, from, next) {
  const accessToken = localStorage.getItem('token');
  if (accessToken) {
    next({ path: '/', replace: true });
  } else {
    next();
  }
}

function publicAdminAuth(to, from, next) {
  const accessToken = localStorage.getItem('admin_token');
  if (accessToken) {
    next({ path: '/admin', replace: true });
  } else {
    next();
  }
}

const routes = [
  {
    path: '/',
    beforeEnter: guardAuth,
    component: () => import('layouts/User.vue'),
    children: [
      {
        path: '/',
        component: () => import('pages/user/Index.vue'),
      },
      {
        path: '/user',
        component: () => import('layouts/UserInfo.vue'),
        children: [
          {
            path: '/user',
            component: () => import('pages/user/MyInformation.vue'),
          },
          {
            path: '/user/notifications',
            component: () => import('pages/user/Notifications.vue'),
          },
          {
            path: '/user/my-orders',
            component: () => import('pages/user/MyOrders.vue'),
          },
          {
            path: '/user/my-contributors',
            component: () => import('pages/user/MyContributors.vue'),
          },
          {
            path: '/user/my-contributor-orders',
            component: () => import('pages/user/MyContributorOrders.vue'),
          },
          {
            path: '/user/my-report',
            component: () => import('pages/user/MyReports.vue'),
          },
          {
            path: '/user/payment-info',
            component: () => import('pages/user/PaymentInfo.vue'),
          },
        ],
      },
      {
        path: '/category/:id',
        component: () => import('pages/user/Category.vue'),
      },
      {
        path: '/search',
        component: () => import('pages/user/Search.vue'),
      },
      {
        path: '/brand/:id',
        component: () => import('pages/user/Brand.vue'),
      },
      {
        path: '/product/:id',
        component: () => import('pages/user/Product.vue'),
      },
    ],
  },
  {
    path: '/login',
    beforeEnter: publicAuth,
    component: () => import('pages/Login.vue'),
  },
  {
    path: '/admin',
    beforeEnter: guardAdminAuth,
    component: () => import('layouts/Admin.vue'),
    children: [
      {
        path: '/admin/user',
        component: () => import('pages/admin/AdminUsers.vue'),
      },
      {
        path: '/admin/notification',
        component: () => import('pages/admin/AdminNotifications.vue'),
      },
      {
        path: '/admin/category',
        component: () => import('pages/admin/AdminCategories.vue'),
      },
      {
        path: '/admin/brand',
        component: () => import('pages/admin/AdminBrand.vue'),
      },
      {
        path: '/admin/product',
        component: () => import('pages/admin/AdminProducts.vue'),
      },
      {
        path: '/admin/order',
        component: () => import('pages/admin/AdminOrders.vue'),
      },
      {
        path: '/admin/contributor-order',
        component: () => import('pages/admin/AdminContributorOrders.vue'),
      },
      {
        path: '/admin/center-order',
        component: () => import('pages/admin/AdminCenterOrders.vue'),
      },
      {
        path: '/admin/report',
        component: () => import('pages/admin/AdminReport.vue'),
      },
      {
        path: '/admin/setting',
        component: () => import('pages/admin/AdminSetting.vue'),
      },
    ],
  },
  {
    path: '/admin/login',
    beforeEnter: publicAdminAuth,
    component: () => import('pages/admin/AdminLogin.vue'),
  },
  {
    path: '/register',
    beforeEnter: publicAuth,
    component: () => import('pages/Register.vue'),
  },
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue'),
  });
}

export default routes;
