export const TABLE_DETAIL_TEXT = {
  id: 'ID',
  fullName: 'Họ tên',
  email: 'Email',
  phoneNumber: 'Số điện thoại',
  referral: 'Người giới thiệu',
  nationalId: 'Chứng minh nhân dân',
  gender: 'Giới tính',
  address: 'Địa chỉ',
  city: 'Thành phố',
  district: 'Quận/huyện',
  ward: 'Phường/xã',
  userRetail: 'Loại hình tham gia',
  orderReceiver: 'Địa điểm giao hàng',
  permanentAddress: 'Địa chỉ thường trú',
  passport: 'Hộ chiếu',
  otherInformation: 'Thông tin khác',
  companyAddress: 'Địa chỉ công ty',
  companyPhone: 'Số điện thoại công ty',
  bankOwner: 'Tên chủ tài khoản',
  bankNumber: 'Số tài khoản',
  bankBranch: 'Chi nhánh',
  status: 'Trạng thái',
  medias: 'Hình ảnh',
  mediaArr: 'Danh sách hình ảnh',
  name: 'Tên',
  sku: 'SKU',
  brand: 'Nhãn hàng',
  category: 'Danh mục',
  price: 'Giá tiền',
  bonusCommission: 'Chiết khấu',
  inventory: 'Tồn kho',
  description: 'Mô tả',
  orderDetails: 'Chi tiết đơn hàng',
  createdAt: 'Ngày tạo',
  updatedAt: 'Ngày cập nhật',
};

export const USER_ROLE = {
  USER: 'USER',
  ADMIN: 'ADMIN',
};

export const USER_STATUS_TEXT = {
  DEACTIVE: 'Chưa kích hoạt',
  ACTIVE: 'Đang hoạt động',
};

export const USER_RETAIL_TEXT = {
  CONTRIBUTOR: 'Thành viên',
  CONTRIBUTOR_EDUCATED: 'Thành viên (đã đi học)',
  SALE: 'Điểm bán hàng',
  DELIVERY: 'Điểm giao hàng',
};

export const USER_GENDER_TEXT = {
  MALE: 'Nam',
  FEMALE: 'Nữ',
};

export const GENDER = {
  MALE: 'MALE',
  FEMALE: 'FEMALE',
};

export const ORDER_STATUS_TEXT = {
  PENDING: 'Đang chờ',
  APPROVED: 'Đã xác nhận',
  DELIVERING: 'Đang giao hàng',
  COMPLETED: 'Đã hoàn tất',
  REJECTED: 'Đã huỷ',
};

export const ORDER_STATUS = {
  PENDING: 'PENDING',
  APPROVED: 'APPROVED',
  DELIVERING: 'DELIVERING',
  COMPLETED: 'COMPLETED',
  REJECTED: 'REJECTED',
};
