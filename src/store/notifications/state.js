export const state = () => ({
  userNotifications: [],
  notifications: [],
  notificationPage: 0,
  totalNotifications: 0,
  fcmToken: '',
});
