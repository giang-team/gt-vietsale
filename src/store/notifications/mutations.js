import _clone from 'lodash/clone';

export const SET_USER_NOTIFICATIONS = 'SET_USER_NOTIFICATIONS';
export const SET_NOTIFICATIONS = 'SET_NOTIFICATIONS';
export const ADD_NOTIFICATION = 'ADD_NOTIFICATION';
export const UPDATE_NOTIFICATION = 'UPDATE_NOTIFICATION';
export const DELETE_NOTIFICATION = 'DELETE_NOTIFICATION';
export const MODIFY_NOTIFICATION = 'MODIFY_NOTIFICATION';
export const MARK_READ = 'MARK_READ';
export const SET_FCM_REGISTRATION_TOKEN = 'SET_FCM_REGISTRATION_TOKEN';

export const mutations = {
  [SET_USER_NOTIFICATIONS](state, notifications) {
    state.userNotifications = notifications;
  },
  [SET_NOTIFICATIONS](state, notificationPagination) {
    state.notifications = notificationPagination.notifications;
    state.notificationPage = notificationPagination.page;
    state.totalNotifications = notificationPagination.total;
  },
  [ADD_NOTIFICATION](state, notification) {
    state.notifications.push(notification);
  },
  [UPDATE_NOTIFICATION](state, notification) {
    const idx = state.notifications.findIndex((p) => p.id === notification.id);
    if (idx > -1) {
      state.notifications = state.notifications.map((p, index) => {
        if (idx === index) {
          return notification;
        }
        return p;
      });
    }
  },
  [DELETE_NOTIFICATION](state, notificationId) {
    state.notifications = state.notifications.filter(
      (p) => p.id !== notificationId,
    );
  },
  [MODIFY_NOTIFICATION](state, notification) {
    const notifications = _clone(state.userNotifications);
    const existNotificationIdx = notifications.findIndex(
      (item) => item.id === notification.id,
    );

    if (existNotificationIdx > -1) {
      notifications[existNotificationIdx] = notification;
    } else {
      notifications.push(notification);
    }

    state.userNotifications = notifications;
  },
  [SET_FCM_REGISTRATION_TOKEN](state, token) {
    state.fcmToken = token;
  },
  [MARK_READ](state, id) {
    state.userNotifications = state.userNotifications.map(
      ({ status, ...noti }) => {
        if (id) {
          if (noti.id === id) {
            return {
              ...noti,
              status: 1,
            };
          }
          return {
            ...noti,
            status,
          };
        }
        return {
          ...noti,
          status: 1,
        };
      },
    );
  },
};
