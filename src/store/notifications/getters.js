export const getters = {
  getNotificationCount(state) {
    return state.userNotifications.filter(n => n.status === 'UNREAD').length;
  }
};
