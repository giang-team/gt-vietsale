export const getters = {
  getProductBrands(state) {
    return state.productBrands.map(({ media, ...data }) => ({
      ...data,
      media: media ? media.path : '',
      mediaArr: media ? [media.path] : [],
    }));
  },
  getProductBrandById(state) {
    return id => state.productBrands.find(brand => brand.id === id);
  }
};
