import {
  SET_PRODUCT_BRANDS,
  MODIFY_PRODUCT_BRAND,
  DELETE_PRODUCT_BRAND,
} from './mutations';

const productBrandFields = `
  id
  name
  media {
    path
  }
  description
  showOnHomepage
  numberOfProducts
  createdAt
`;

export const actions = {
  async getProductBrands({ commit }, options = {}) {
    const {
      filter = '',
      page = 0,
      limit = 10,
      orderBy = '',
      order = '',
    } = options;

    const result = await this._vm.$apollo.query(`
      {
        getProductBrands(pagination: {
          page: ${page},
          limit: ${limit},
          orderBy: "${orderBy}",
          order: "${order}"
        }, filters: { generic: "${filter}" }) {
          brands {
            ${productBrandFields}
          }
          page
          total
        }
      }
    `);

    if (result && result.getProductBrands) {
      commit(SET_PRODUCT_BRANDS, result.getProductBrands);
    }
  },
  async createProductBrand(
    { commit },
    { name = '', mediaArr = [], description = '', showOnHomepage = true },
  ) {
    const mediaImg = mediaArr.length ? mediaArr[0] : '';
    const result = await this._vm.$apollo.mutation(`
      mutation {
        createProductBrand(input: {
          name: "${name}",
          media: "${mediaImg}",
          description: "${description}",
          showOnHomepage: ${showOnHomepage}
        }) {
          ${productBrandFields}
        }
      }
    `);

    if (result && result.createProductBrand) {
      commit(MODIFY_PRODUCT_BRAND, result.createProductBrand);
      this._vm.$notify.success('Tạo nhãn hàng mới thành công.');
    }
  },
  async updateProductBrand(
    { commit },
    { id, name, mediaArr, description, showOnHomepage },
  ) {
    const mediaImg = mediaArr.length ? mediaArr[0] : '';
    const result = await this._vm.$apollo.mutation(`
      mutation {
        updateProductBrand(id: ${id}, input: {
          ${name ? `name: "${name}",` : ''}
          ${mediaImg ? `media: "${mediaImg}",` : ''}
          ${description ? `description: "${description}",` : ''}
          showOnHomepage: ${showOnHomepage}
        }) {
          ${productBrandFields}
        }
      }
    `);

    if (result && result.updateProductBrand) {
      commit(MODIFY_PRODUCT_BRAND, result.updateProductBrand);
      this._vm.$notify.success('Chỉnh sửa nhãn hàng thành công.');
    }
  },
  async deleteProductBrand({ commit }, id) {
    const result = await this._vm.$apollo.mutation(`
      mutation {
        deleteProductBrand(id: ${id})
      }
    `);

    if (result && result.deleteProductBrand) {
      commit(DELETE_PRODUCT_BRAND, id);
      this._vm.$notify.success('Xoá nhãn hàng thành công.');
    }
  },
};
