export const SET_USER = 'SET_USER';
export const SET_TOKEN = 'SET_TOKEN';
export const RESET_WHEN_LOGOUT = 'RESET_WHEN_LOGOUT';

export const mutations = {
  [SET_USER](state, user) {
    state.user = Object.assign({}, user);
  },
  [SET_TOKEN](state, data) {
    state.accessToken = data;
  },
  [RESET_WHEN_LOGOUT](state) {
    state.accessToken = '';
    state.user = {};
  },
};
