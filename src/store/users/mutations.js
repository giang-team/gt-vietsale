export const SET_USERS = 'SET_USERS';
export const SET_USER_PAGINATION = 'SET_USER_PAGINATION';
export const VERIFY_USER = 'VERIFY_USER';
export const MODIFY_USER = 'MODIFY_USER';
export const UPDATE_USER_RETAIL = 'UPDATE_USER_RETAIL';
export const DELETE_USER = 'DELETE_USER';

export const mutations = {
  [SET_USERS](state, users) {
    state.users = users;
  },
  [SET_USER_PAGINATION](state, userPagination) {
    state.users = userPagination.users;
    state.userPage = userPagination.page;
    state.totalUsers = userPagination.total;
  },
  [VERIFY_USER](state, userResponse) {
    const users = [...state.users];
    const idx = users.findIndex((user) => user.id === userResponse.id);
    if (idx > -1) {
      users[idx].status = userResponse.status;
      users[idx].validator = userResponse.validator;
    }
    state.users = users;
  },
  [MODIFY_USER](state, user) {
    const listUsers = [...state.users];
    const idx = listUsers.findIndex((u) => u.id === user.id);
    if (idx > -1) {
      listUsers[idx] = user;
    } else {
      listUsers.push(user);
    }

    state.users = listUsers;
  },
  [UPDATE_USER_RETAIL](state, userResponse) {
    const users = [...state.users];
    const idx = users.findIndex((user) => user.id === userResponse.id);
    if (idx > -1) {
      users[idx].userRetail = userResponse.userRetail;
    }
    state.users = users;
  },
  [DELETE_USER](state, userId) {
    state.users = state.users.filter((u) => u.id !== userId);
  },
};
