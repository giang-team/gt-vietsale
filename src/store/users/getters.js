export const getters = {
  getContributorsByUser(state) {
    return state.users.map((user) => ({
      ...user,
      hideUpdate: true,
    }));
  },
};
