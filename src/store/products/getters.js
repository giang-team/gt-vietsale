export const getters = {
  getProducts(state) {
    return state.products.map(({ medias, thumbnail, ...data }) => ({
      ...data,
      media: thumbnail ? thumbnail.path : '',
      mediaArr: medias && medias.length > 0 ? medias.map(media => media.path) : [],
      brandObj: data.brand ? { value: data.brand.id, label: data.brand.name } : null,
      categoryObj: data.category ? { value: data.category.id, label: data.category.name } : null,
      bonusCommissionForContributor: data.bonusCommission ? data.bonusCommission.CONTRIBUTOR : 0,
      bonusCommissionForContributorEducated: data.bonusCommission ? data.bonusCommission.CONTRIBUTOR_EDUCATED : 0,
      bonusCommissionForSale: data.bonusCommission ? data.bonusCommission.SALE : 0,
      bonusCommissionForDelivery: data.bonusCommission ? data.bonusCommission.DELIVERY : 0,
    }));
  }
};
