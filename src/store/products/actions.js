import _get from 'lodash/get';
import { SET_PRODUCTS, DELETE_PRODUCT, MODIFY_PRODUCT } from './mutations';
import { CommonUtils } from '../../utils/commons';

export const actions = {
  async getProducts({ commit }, options = {}) {
    const {
      filters = {},
      filter = '',
      page = 0,
      limit = 10,
      orderBy = '',
      order = '',
    } = options;

    const filtersText = Object.keys(filters).map((key) => {
      let text = '';
      switch (key) {
        case 'brand':
        case 'category':
          text = `${key}: ${filters[key]},`;
          break;
        default:
          text = `${key}: "${filters[key]}",`;
      }
      return text;
    });

    const result = await this._vm.$apollo.query(`
      {
        getProducts(
          pagination: {
            page: ${page},
            limit: ${limit},
            orderBy: "${orderBy}",
            order: "${order}"
          },
          filters: {
            generic: "${filter}",
            ${filtersText}
          }
        ) {
          products {
            id
            name
            sku
            price
            vat
            bonusCommission {
              CONTRIBUTOR
              CONTRIBUTOR_EDUCATED
              SALE
              DELIVERY
            }
            inventory
            shortDescription
            description
            medias {
              path
            }
            thumbnail {
              path
            }
            brand {
              id
              name
              media {
                path
              }
            }
            category {
              id
              name
              media {
                path
              }
            }
            createdAt
            updatedAt
          }
          page
          total
        }
      }
    `);

    if (result && result.getProducts) {
      commit(SET_PRODUCTS, result.getProducts);
    }
  },
  async getProductById(_, id) {
    const result = await this._vm.$apollo.query(`
      {
        getProductById(id: ${id}) {
          id
          name
          sku
          price
          vat
          bonusCommission {
            CONTRIBUTOR
            CONTRIBUTOR_EDUCATED
            SALE
            DELIVERY
          }
          inventory
          shortDescription
          description
          medias {
            path
          }
          brand {
            id
            name
            media {
              path
            }
          }
          category {
            id
            name
            media {
              path
            }
          }
          createdAt
          updatedAt
        }
      }
    `);

    if (result && result.getProductById) {
      return result.getProductById;
    }
    return null;
  },
  async createProduct({ commit }, product = {}) {
    const inputList = [
      'name',
      'sku',
      'price',
      'bonusCommissionForContributor',
      'bonusCommissionForContributorEducated',
      'bonusCommissionForSale',
      'bonusCommissionForDelivery',
      'inventory',
      'mediaArr',
      'brandObj',
      'categoryObj',
      'shortDescription',
      'description',
    ]
      .map((key) => {
        switch (key) {
          case 'categoryObj':
            return `category: ${_get(product[key], 'value', '""')}`;
          case 'brandObj':
            return `brand: ${_get(product[key], 'value', '""')}`;
          case 'mediaArr':
            return `medias: [${(product[key] || [])
              .map((media) => `"${media}"`)
              .join(',')}]`;
          case 'inventory':
          case 'bonusCommissionForContributor':
          case 'bonusCommissionForContributorEducated':
          case 'bonusCommissionForSale':
          case 'bonusCommissionForDelivery':
          case 'price':
            return `${key}: ${product[key] || '0'}`;
          default:
            return `${key}: "${CommonUtils.escapeHtml(product[key]) || ''}"`;
        }
      })
      .filter((i) => i !== null);

    const result = await this._vm.$apollo.mutation(`
      mutation {
        createProduct(input: { ${inputList} }) {
          id
          name
          sku
          price
          vat
          shortDescription
          description
          bonusCommission {
            CONTRIBUTOR
            CONTRIBUTOR_EDUCATED
            SALE
            DELIVERY
          }
          inventory
          medias {
            path
          }
          thumbnail {
            path
          }
          brand {
            id
            name
            media {
              path
            }
          }
          category {
            id
            name
            media {
              path
            }
          }
          createdAt
          updatedAt
        }
      }
    `);

    if (result && result.createProduct) {
      commit(MODIFY_PRODUCT, result.createProduct);
      this._vm.$notify.success('Tạo sản phẩm mới thành công.');
    }
  },
  async updateProduct({ commit }, product = {}) {
    const inputList = [
      'name',
      'sku',
      'price',
      'bonusCommissionForContributor',
      'bonusCommissionForContributorEducated',
      'bonusCommissionForSale',
      'bonusCommissionForDelivery',
      'inventory',
      'mediaArr',
      'brandObj',
      'categoryObj',
      'shortDescription',
      'description',
    ]
      .map((key) => {
        if (product[key]) {
          switch (key) {
            case 'categoryObj':
              return `category: ${_get(product[key], 'value', '""')}`;
            case 'brandObj':
              return `brand: ${_get(product[key], 'value', '""')}`;
            case 'mediaArr':
              return `medias: [${(product[key] || [])
                .map((media) => `"${media}"`)
                .join(',')}]`;
            case 'price':
            case 'inventory':
            case 'bonusCommissionForContributor':
            case 'bonusCommissionForContributorEducated':
            case 'bonusCommissionForSale':
            case 'bonusCommissionForDelivery':
              return `${key}: ${product[key] || 'null'}`;
            default:
              return `${key}: "${CommonUtils.escapeHtml(product[key]) || ''}"`;
          }
        }
        return null;
      })
      .filter((i) => i !== null);

    const result = await this._vm.$apollo.mutation(`
      mutation {
        updateProduct(id: ${product.id}, input: { ${inputList} }) {
          id
          name
          sku
          price
          vat
          shortDescription
          description
          bonusCommission {
            CONTRIBUTOR
            CONTRIBUTOR_EDUCATED
            SALE
            DELIVERY
          }
          inventory
          medias {
            path
          }
          thumbnail {
            path
          }
          brand {
            id
            name
            media {
              path
            }
          }
          category {
            id
            name
            media {
              path
            }
          }
          createdAt
          updatedAt
        }
      }
    `);

    if (result && result.updateProduct) {
      commit(MODIFY_PRODUCT, result.updateProduct);
      this._vm.$notify.success('Chỉnh sửa sản phẩm thành công.');
    }
  },
  async deleteProduct({ commit }, id) {
    const result = await this._vm.$apollo.mutation(`
      mutation {
        deleteProduct(id: ${id})
      }
    `);

    if (result && result.deleteProduct) {
      commit(DELETE_PRODUCT, id);
      this._vm.$notify.success('Xoá sản phẩm thành công.');
    }
  },
};
