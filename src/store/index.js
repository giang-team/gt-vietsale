import Vue from 'vue';
import Vuex from 'vuex';
import auth from './auth';
import products from './products';
import users from './users';
import notifications from './notifications';
import productCategories from './productCategories';
import productBrands from './productBrands';
import cart from './cart';
import orders from './orders';
import resources from './resources';
import settings from './settings';
import reports from './reports';

// import example from './module-example'

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default new Vuex.Store({
  modules: {
    auth,
    products,
    users,
    notifications,
    productCategories,
    productBrands,
    cart,
    orders,
    resources,
    settings,
    reports
  },
  strict: false,
});
