import { SET_SETTINGS } from './mutations';

export const actions = {
  async getSettings({ commit }) {
    const result = await this._vm.$apollo.query(`
      {
        getSettings {
          settingName
          settingValue
        }
      }
    `);

    if (result && result.getSettings) {
      commit(SET_SETTINGS, result.getSettings);
    }
  },
  async updateSettings(_, settings = []) {
    const inputList = settings.map(
      ({ settingName, settingValue }) => `{settingName: "${settingName}", settingValue: "${settingValue}"}`
    );

    const result = await this._vm.$apollo.mutation(`
      mutation {
        updateSettings(input: [${inputList}])
      }
    `);

    if (result && result.updateSettings) {
      this._vm.$notify.success('Lưu cài đặt thành công.');
    }
  },
};
