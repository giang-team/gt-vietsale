export const SET_SETTINGS = 'SET_SETTINGS';

export const mutations = {
  [SET_SETTINGS](state, settings) {
    state.settings = settings;
  },
};
