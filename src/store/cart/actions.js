import _clone from 'lodash/clone';
import { ADD_TO_CART, REMOVE_CART_ITEM, INCREASE_CART_QUANTITY, DECREASE_CART_QUANTITY, CLEAR_CART } from './mutations';

export const actions = {
  addToCart({ commit, rootState }, { itemId, quantity = 1 }) {
    const item = _clone(rootState.products.products.find(p => p.id === itemId));
    if (item) {
      commit(ADD_TO_CART, { item, quantity });
      this._vm.$notify.success('Đã thêm vào giỏ hàng.');
    }
  },
  removeCartItem({ commit }, itemId) {
    commit(REMOVE_CART_ITEM, itemId);
  },
  clearCart({ commit }) {
    commit(CLEAR_CART);
  },
  increaseCartQuantity({ commit }, itemId) {
    commit(INCREASE_CART_QUANTITY, itemId);
  },
  decreaseCartQuantity({ commit }, itemId) {
    commit(DECREASE_CART_QUANTITY, itemId);
  }
};
