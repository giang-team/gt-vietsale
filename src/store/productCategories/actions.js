import {
  SET_PRODUCT_CATEGORIES,
  MODIFY_PRODUCT_CATEGORY,
  DELETE_PRODUCT_CATEGORY,
} from './mutations';

const productCategoryFields = `
  id
  name
  media {
    path
  }
  showOnHomepage
  numberOfProducts
  createdAt
`;

export const actions = {
  async getProductCategories({ commit }, options = {}) {
    const {
      filter = '',
      page = 0,
      limit = 10,
      orderBy = '',
      order = '',
    } = options;

    const result = await this._vm.$apollo.query(`
      {
        getProductCategories(pagination: {
          page: ${page},
          limit: ${limit},
          orderBy: "${orderBy}",
          order: "${order}"
        }, filters: { generic: "${filter}" }) {
          categories {
            ${productCategoryFields}
          }
          page
          total
        }
      }
    `);

    if (result && result.getProductCategories) {
      commit(SET_PRODUCT_CATEGORIES, result.getProductCategories);
    }
  },
  async createProductCategory(
    { commit },
    { name = '', mediaArr = [], showOnHomepage = true },
  ) {
    const mediaImg = mediaArr.length ? mediaArr[0] : '';
    const result = await this._vm.$apollo.mutation(`
      mutation {
        createProductCategory(input: {name: "${name}", media: "${mediaImg}", showOnHomepage: ${showOnHomepage}}) {
          ${productCategoryFields}
        }
      }
    `);

    if (result && result.createProductCategory) {
      commit(MODIFY_PRODUCT_CATEGORY, result.createProductCategory);
      this._vm.$notify.success('Tạo danh mục sản phẩm mới thành công.');
    }
  },
  async updateProductCategory(
    { commit },
    { id, name, mediaArr, showOnHomepage },
  ) {
    const mediaImg = mediaArr.length ? mediaArr[0] : '';
    const result = await this._vm.$apollo.mutation(`
      mutation {
        updateProductCategory(id: ${id}, input: {
          ${name ? `name: "${name}",` : ''}
          ${mediaImg ? `media: "${mediaImg}",` : ''}
          showOnHomepage: ${showOnHomepage}
        }) {
          ${productCategoryFields}
        }
      }
    `);

    if (result && result.updateProductCategory) {
      commit(MODIFY_PRODUCT_CATEGORY, result.updateProductCategory);
      this._vm.$notify.success('Chỉnh sửa danh mục sản phẩm thành công.');
    }
  },
  async deleteProductCategory({ commit }, id) {
    const result = await this._vm.$apollo.mutation(`
      mutation {
        deleteProductCategory(id: ${id})
      }
    `);

    if (result && result.deleteProductCategory) {
      commit(DELETE_PRODUCT_CATEGORY, id);
      this._vm.$notify.success('Xoá danh mục sản phẩm thành công.');
    }
  },
};
