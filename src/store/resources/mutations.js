export const SET_CITIES = 'SET_CITIES';

export const mutations = {
  [SET_CITIES](state, cities) {
    state.cities = cities;
  },
};
