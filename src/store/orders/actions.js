import { MODIFY_ORDER, SET_ORDERS, DELETE_ORDER } from './mutations';

const returnSchema = `
  id
  user {
    id
    fullName
  }
  orderReceiver {
    fullAddress
  }
  subtotal
  total
  totalVat
  totalBonusCommission
  totalEarning
  shippingFee
  fullName
  phoneNumber
  note
  isDeliverToCenter
  status
  createdAt
  orderDetails {
    product {
      id
      name
      thumbnail {
        path
      }
    }
    price
    vat
    bonusCommission
    earning
    quantity
  }`;

export const actions = {
  async getOrders(
    { commit },
    {
      isOwner = false,
      isReceiver = false,
      isAdmin = false,
      isDeliverToCenter = false,
      userRetails = [],
      options = {},
    } = {},
  ) {
    const {
      filter = '',
      page = 0,
      limit = 10,
      orderBy = '',
      order = '',
    } = options;

    const result = await this._vm.$apollo.query(`
      {
        getOrders(pagination: {
          page: ${page},
          limit: ${limit},
          orderBy: "${orderBy}",
          order: "${order}"
        }, filters: {
          ${filter ? `generic: "${filter}",` : ''}
          ${isOwner ? `isOwner: ${isOwner},` : ''}
          ${isReceiver ? `isReceiver: ${isReceiver},` : ''},
          ${isAdmin ? `isAdmin: ${isAdmin},` : ''},
          ${
            isDeliverToCenter ? `isDeliverToCenter: ${isDeliverToCenter},` : ''
          },
          ${userRetails.length ? `userRetails: [${userRetails}]` : ''}
        }) {
          orders {
            ${returnSchema}
          }
          page
          total
        }
      }
    `);

    if (result && result.getOrders) {
      commit(SET_ORDERS, result.getOrders);
    }
  },
  async createMyOrder({ commit, rootGetters }, order = {}) {
    const orderDetailText = rootGetters['cart/getCart'].reduce(
      (text, detail) => {
        text += `{product: ${detail.id}, quantity: ${detail.quantity}},`;
        return text;
      },
      '',
    );

    const inputList = Object.keys(order).map((key) => {
      switch (key) {
        case 'orderReceiver':
          return `${key}: ${order[key] ? order[key].value : '""'}`;
        default:
          return `${key}: "${order[key] || ''}"`;
      }
    });
    inputList.push(`orderDetails: [${orderDetailText}]`);

    const result = await this._vm.$apollo.mutation(`
      mutation {
        createMyOrder(input: { ${inputList} }) {
          ${returnSchema}
        }
      }
    `);

    if (result && result.createMyOrder) {
      commit(MODIFY_ORDER, result.createMyOrder);
      this._vm.$notify.success('Tạo đơn hàng mới thành công.');
      return result.createMyOrder;
    }
    return false;
  },
  async updateOrder({ commit }, { id, status = '', shippingFee = '' }) {
    const result = await this._vm.$apollo.mutation(`
      mutation {
        updateOrder(id: ${id}, input: {
          ${status !== '' ? `status: ${status},` : ''}
          ${shippingFee !== '' ? `shippingFee: ${shippingFee},` : ''}
        }) {
          ${returnSchema}
        }
      }
    `);

    if (result && result.updateOrder) {
      commit(MODIFY_ORDER, result.updateOrder);
      this._vm.$notify.success('Cập nhật đơn hàng thành công.');
      return true;
    }
    return false;
  },
  async deliverToCenter({ commit }, id) {
    const result = await this._vm.$apollo.mutation(`
      mutation {
        deliveryToCenter(id: ${id}) {
          ${returnSchema}
        }
      }
    `);

    if (result && result.deliveryToCenter) {
      commit(MODIFY_ORDER, result.deliveryToCenter);
      this._vm.$notify.success('Gửi đơn hàng về trung tâm thành công.');
      return true;
    }
    return false;
  },
  async rejectMyOrder({ commit }, id) {
    const result = await this._vm.$apollo.mutation(`
      mutation {
        rejectMyOrder(id: ${id}) {
          ${returnSchema}
        }
      }
    `);

    if (result && result.rejectMyOrder) {
      commit(MODIFY_ORDER, result.rejectMyOrder);
      this._vm.$notify.success('Huỷ đơn hàng thành công.');
      return true;
    }
    return false;
  },
  async deleteOrder({ commit }, id) {
    const result = await this._vm.$apollo.mutation(`
      mutation {
        deleteOrder(id: ${id})
      }
    `);

    if (result && result.deleteOrder) {
      commit(DELETE_ORDER, id);
      this._vm.$notify.success('Xoá đơn hàng thành công.');
    }
  },
};
