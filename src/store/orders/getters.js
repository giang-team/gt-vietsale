export const getters = {
  getContributorOrders(state) {
    return state.orders.map((order) => ({
      ...order,
      hideUpdate: order.isDeliverToCenter,
    }));
  },
};
