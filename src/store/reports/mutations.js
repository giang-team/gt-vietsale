export const SET_ORDER_REPORTS = 'SET_ORDER_REPORTS';

export const mutations = {
  [SET_ORDER_REPORTS](state, orderReports) {
    state.orderReports = orderReports;
  },
};
