import { SET_ORDER_REPORTS } from './mutations';

export const actions = {
  async getOrderReports({ commit }, filters = {}) {
    const filterText = Object.keys(filters).map(key => {
      switch (key) {
        case 'dateRange':
          return `dateRange: [${(filters[key] || []).map(date => `"${date}"`).join(',')}]`;
        case 'isOwner':
          return `isOwner: ${filters[key]}`;
        default:
          return '';
      }
    });

    const result = await this._vm.$apollo.query(`
      {
        getOrderReports(filters: { ${filterText} }) {
          date
          totalSold
          product {
            name
            sku
            brand {
              name
            }
            category {
              name
            }
          }
        }
      }
    `);

    if (result && result.getOrderReports) {
      commit(SET_ORDER_REPORTS, result.getOrderReports);
    }
  },
};
